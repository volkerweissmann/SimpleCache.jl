ENV["JULIA_DEBUG"] = Main;
function recstring(value)
    ret = string(typeof(value)) * " = " * string(value) * "\n"
    for el in fieldnames(typeof(value))
        ret *= string(el) * " = " * recstring(getfield(value, el)) * "\n"
    end
    ret
end
