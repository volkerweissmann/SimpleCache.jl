# You cannot start or import this file directly, use runtest.jl instead
using InteractiveUtils
using CodeTracking
include("../src/SimpleCache.jl")

@noinline function multi(x::Int)
    println("printing stuff 1")
    @debug "debug macro 1"
    return VALUE_A
end

@noinline function multi(x)
    println("printing stuff 2")
    @debug "debug macro 2"
    return VALUE_B
end

@noinline function caller(x)
    multi(x)
    return 5
end

global glob = VALUE_C
@noinline function use_global()
    return glob
end

const constglob = VALUE_D
@noinline function use_const()
    return constglob
end

@noinline function ya_func(x)
    return VALUE_E
end

@noinline function pointer(x)
    if rand() > 0.5
        ptr = multi
    else
        ptr = ya_func
    end
    return ptr(x)
end

println(SimpleCache.hash_function_recursive([], Meta.parse(@code_string(caller(4)))))
println(SimpleCache.hash_function_recursive([], Meta.parse(@code_string(caller(4.0)))))
println(SimpleCache.hash_function_recursive([], Meta.parse(@code_string(use_global()))))
println(SimpleCache.hash_function_recursive([], Meta.parse(@code_string(use_const()))))
println(SimpleCache.hash_function_recursive([], Meta.parse(@code_string(pointer(4)))))